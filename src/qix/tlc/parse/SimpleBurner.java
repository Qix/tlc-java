package qix.tlc.parse;

import java.io.IOException;
import java.io.InputStream;

/**
 * Adapter class that
 * performs a boolean callback on the 
 * stream and automatically/interally
 * builds the return string for the burn()
 * implementation
 * 
 * @author Qix
 *
 */
abstract class SimpleBurner implements IBurner {

	private final boolean processCurrent;
	
	SimpleBurner(boolean processCurrent)
	{
		// Store
		this.processCurrent = processCurrent;
	}
	
	/**
	 * Denotes what should happen with the
	 * next burning operation
	 * 
	 * @author Qix
	 *
	 */
	enum BurnAction
	{
		/**
		 * Add character and burn/test another character
		 */
		CONTINUE,
		
		/**
		 * Add character but finish
		 */
		FINISH,
		
		/**
		 * Don't add the character and stop
		 */
		ABORT,
		
		/**
		 * Don't add the character and stop; re-process last character
		 */
		ABORTRESIDUAL,
	}
	
	@Override
	public BurnResult burn(InputStream stream, char currentCharacter) throws IOException, ParseException {
		// Create string buffer
		//	and current char placeholder
		StringBuffer buf = new StringBuffer();
		char cur = (char) -1;
		char residual = (char) -1;
		
		// Process current character?
		boolean currentAborted = false;
		if(this.processCurrent)
		{
			// Get action and switch
			switch(this.getAction(currentCharacter))
			{
			case CONTINUE:
				// Add character and break
				buf.append(currentCharacter);
				break;
			case FINISH:
				// Add the character and fall through
				buf.append(currentCharacter);
			case ABORT:
				// Flag for abortion
				currentAborted = true;
				break;
			case ABORTRESIDUAL:
				// Flag for abortion and set residual character
				currentAborted = true;
				residual = currentCharacter;
				break;
			}
		}
		
		// Did we abort the first?
		if(!currentAborted)
		{
			// Burn
			burnLoop:
			while((cur = (char)stream.read()) != (char) -1)
			{
				// Get action and switch
				switch(this.getAction(cur))
				{
				case CONTINUE:
					// Add character and continue
					buf.append(cur);
					continue;
				case FINISH:
					// Add the character and fall through
					buf.append(cur);
				case ABORT:
					// Kill the loop
					break burnLoop;
				case ABORTRESIDUAL:
					// Set residual and kill loop
					residual = cur;
					break burnLoop;
				}
			}
		}
		
		// Finish
		this.onFinish(stream, cur == (char)-1, cur);
		
		// Return
		return new BurnResult(buf.toString(), residual);
	}
	
	/**
	 * Evaluates whether or not a character
	 * is a valid part of this return block
	 * and determines what to do with it
	 * @param cha The character being burnt
	 * @return The action that should be taken with the character
	 */
	abstract BurnAction getAction(char cha);
	
	/**
	 * Called when isApplicable returns false,
	 * before burn() returns or EOL is reached.
	 * @param stream The stream being burnt
	 * @param hard True if it was a 'hard' (EOL) stop or false if isApplicable() returned false ('soft')
	 * @param lastChar The last character that was burnt
	 * @throws ParseException Thrown when something goes wrong whilst parsing
	 */
	void onFinish(InputStream stream, boolean hard, char lastChar) throws ParseException {}
}
