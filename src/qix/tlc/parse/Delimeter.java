package qix.tlc.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Lists the types of delimiters we can burn to along with their specific
 * information on how to handle the following block of data
 * 
 * @author Qix
 * 
 */
enum Delimiter {
	FUNCTION('@', new Delimiter.LookBehindComparator() {

		@Override
		public boolean matches(char[] buf) {
			// Get second to last
			char stl = buf[buf.length - 2];

			// Return
			return stl == '\n' || stl == '\r' || stl == (char) -1;
		}

		@Override
		public int getMaxBuffer() {
			// Return
			return 1;
		}

	}, new LineBurner(false), new Delimiter.VisitProxy() {

		@Override
		public String visit(ITLCVisitor visitor, String data, int line, int col)
				throws ParseException {
			// Kill any comments
			data = data.replaceAll("\\s*\\/\\/.*$", "");

			// Setup matcher
			boolean hasFoundFunction = false;
			String function = null;
			List<String> args = new ArrayList<>();
			Matcher matcher = Delimiter.SPLIT_SPACE_REGEX.matcher(data);

			// Find all
			while (matcher.find()) {
				// Have we found a function yet?
				if (!hasFoundFunction) {
					hasFoundFunction = true;
					function = matcher.group(1).replace("\"", "");
				} else
					args.add(matcher.group(1).replace("\"", ""));
			}

			// Proxy + Return
			return visitor.onFunction(function, args, line);
		}

	}),

	INVOCATION('{', new Delimiter.LookBehindComparator() {

		@Override
		public boolean matches(char[] buf) {
			// Get second to last
			char stl = buf[buf.length - 2];

			// Return
			return stl == '$';
		}

		@Override
		public int getMaxBuffer() {
			// Return
			return 1;
		}

	}, new BlockBurner(false), new Delimiter.VisitProxy() {
		@Override
		public String visit(ITLCVisitor visitor, String data, int line, int col) {
			// Proxy + Return
			return visitor.onInvocation(data, line, col);
		}
	}),

	INVOCATION_REPLACEMENT('[', new Delimiter.LookBehindComparator() {

		@Override
		public boolean matches(char[] buf) {
			// Return
			return Delimiter.matchesWithOffset(buf,
					Delimiter.INVOCATION_REPLACEMENT_SEQUENCE);
		}

		@Override
		public int getMaxBuffer() {
			// Return
			return 2;
		}

	}, new InvocationBlockBurner(false), new Delimiter.VisitProxy() {
		@Override
		public String visit(ITLCVisitor visitor, String data, int line, int col)
				throws ParseException {
			// Check format
			Matcher matcher = Delimiter.INVOCATION_REPLACEMENT_PATTERN
					.matcher(data);
			if (!matcher.matches())
				// Throw
				throw new ParseException(
						line,
						col,
						"Invalid format for invocation-replacement block; should be $[[function_name parametertext...]]");

			// Proxy + Return
			return visitor.onInvocationReplacement(matcher.group(1),
					matcher.group(2), line, col);
		}
	}, 1),

	REPLACEMENT('$', null, new WBurner(true), new Delimiter.VisitProxy() {
		@Override
		public String visit(ITLCVisitor visitor, String data, int line, int col) {
			// Proxy + Return
			return visitor.onReplacement(data, line, col);
		}
	}),

	;

	/**
	 * Special regex for splitting up strings by spaces while honoring quoted
	 * strings
	 */
	static final Pattern SPLIT_SPACE_REGEX = Pattern
			.compile("([^\"]\\S*|\".+?\")\\s*");

	static final char[] INVOCATION_REPLACEMENT_SEQUENCE = new char[] { '$', '[' };
	static final Pattern INVOCATION_REPLACEMENT_PATTERN = Pattern
			.compile("^\\s*(\\w+)(.+)][\\s\\r\\n]*$", Pattern.DOTALL);

	/**
	 * The actual delimiter character
	 */
	final char delim;

	/**
	 * The previous characters that must match, or null for no comparator
	 */
	final Delimiter.LookBehindComparator comparator;

	/**
	 * The object that handles getting the applicable block of data to send to
	 * the visitor
	 */
	final IBurner burner;

	/**
	 * The visitor proxy to use to route visitation calls
	 */
	final VisitProxy proxy;

	/**
	 * The number of characters to back up in the final buffer to compensate for
	 * multiple-character delimiters
	 */
	final int backup;

	Delimiter(char delim, Delimiter.LookBehindComparator comparator,
			IBurner burner, VisitProxy proxy) {
		// This
		this(delim, comparator, burner, proxy, 0);
	}

	Delimiter(char delim, Delimiter.LookBehindComparator comparator,
			IBurner burner, VisitProxy proxy, int backup) {
		// Store
		this.delim = delim;
		this.comparator = comparator;
		this.burner = burner;
		this.proxy = proxy;
		this.backup = backup;

		// Validate Length
		if (comparator != null
				&& comparator.getMaxBuffer() >= TLCParser.LOOKBEHIND - 1)
			throw new RuntimeException(
					String.format(
							"Max lookbehind buffer size for delimiter '%c' is bigger than lookbehind value; "
									+ "increase lookbehind value!", delim));
	}

	@Override
	public String toString() {
		// Return
		return Character.toString(this.delim);
	}

	/**
	 * Comparator interface that compares the given buffer to one or more
	 * specific other buffers
	 * 
	 * @author Qix
	 * 
	 */
	static interface LookBehindComparator {
		/**
		 * Checks to see if the character buffer matches this criteria
		 * 
		 * @param buf
		 *            The buffer to compare against (Always TLCParser.this.buf)
		 * @return Returns whether or not the criteria is met
		 */
		boolean matches(char[] buf);

		/**
		 * @return The minimal amount of lookbehind+1 this parser must support
		 */
		int getMaxBuffer();
	}

	/**
	 * Checks two character buffers and checks to see if the secondary matches
	 * the end of the source with an offset of -1.
	 * 
	 * Example: abcde bcd -> matches
	 * 
	 * abcde cd -> matches
	 * 
	 * abcde abcd -> matches
	 * 
	 * abcde abcde -> error
	 * 
	 * Assumption: source.length > secondary.length
	 * 
	 * @param source
	 *            The source buffer to compare against
	 * @param secondary
	 *            The secondary buffer to compare
	 * @return Whether or not the source ends in secondary matches (with offset
	 *         of -1)
	 */
	static boolean matchesWithOffset(char[] source, char[] secondary) {
		// Cache secondary length
		int secondaryLen = secondary.length;

		// Calculate start offset
		int startOffset = source.length - (secondaryLen + 1);

		// Iterate
		for (int i = 0; i < secondaryLen; i++)
			// Validate
			if (source[i + startOffset] != secondary[i])
				return false;

		// Return
		return true;
	}

	/**
	 * Returns a delimiter given its character
	 * 
	 * This is a little special: we *must always* test for replacement last.
	 * 
	 * @param cha
	 *            The character to look for
	 * @param previous
	 *            The previous character (used for {@link
	 *            Delimiter.REPLACEMENT#})
	 * @return The delimiter corresponding to that character, or null if not
	 *         found
	 */
	public static Delimiter getByChar(char cha, char previous) {
		// Iterate
		for (Delimiter d : Delimiter.values())
			// Check + Return
			if (d != Delimiter.REPLACEMENT && d.delim == cha)
				return d;

		// Test REPLACEMENT
		if (Delimiter.REPLACEMENT.delim == previous)
			// Return
			return Delimiter.REPLACEMENT;

		// Return
		return null;
	}

	/**
	 * Defines a proxy method prototype for visitor routing so I don't have to
	 * deal with black-magic voodoo reflection.
	 * 
	 * @author Qix
	 * 
	 */
	static interface VisitProxy {
		/**
		 * Called by the parser in order to proxy the call to the correct
		 * method.
		 * 
		 * @param visitor
		 *            The visitor to redirect to
		 * @param data
		 *            The data to post
		 * @param line
		 *            The line on which this call originated
		 * @param col
		 *            The column at which this call originated
		 * @return The return value of the visitor call
		 * @throws ParseException
		 *             Thrown when something bad happens whilst parsing
		 */
		public String visit(ITLCVisitor visitor, String data, int line, int col)
				throws ParseException;
	}
}