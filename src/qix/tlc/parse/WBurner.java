package qix.tlc.parse;


/**
 * A burner used for replacements (the '$' without
 * "{...}" delim)
 * 
 * Essentially returns the next "\w+" from the stream
 * 
 * @author Qix
 *
 */
class WBurner extends SimpleBurner {

	WBurner(boolean processCurrent)
	{
		// Super
		super(processCurrent);
	}
	
	@Override
	BurnAction getAction(char cha) {
		// Is it a valid character?
		if(Character.toString(cha).matches("\\w"))
			return BurnAction.CONTINUE;
		else
			return BurnAction.ABORTRESIDUAL;
	}
}
