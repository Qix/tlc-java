package qix.tlc.parse;

import java.util.List;

/**
 * Defines prototype callback methods
 * for the parser to call when TLC-related
 * elements are discovered.
 * 
 * @author Qix
 *
 */
public interface ITLCVisitor {
	/**
	 * Called when an invocation block is
	 * to be processed
	 * @param block The block to process
	 * @param line The line on which this call originated
	 * @param col The column at which this call originated
	 * @return The replacement text (or null for none)
	 */
	public String onInvocation(String block, int line, int col);
	
	/**
	 * Called when a replacement is
	 * to be processed
	 * @param symbol The symbol to process
	 * @param line The line on which this call originated
	 * @param col The column at which this call originated
	 * @return The replacement text (or null for none)
	 */
	public String onReplacement(String symbol, int line, int col);
	
	/**
	 * Called when a function is to be executed
	 * @param function The function to call
	 * @param args A list of arguments (never null; no args = size==0)
	 * @param line The line on which this call originated
	 * @return The replacement text (or null for none)
	 * @throws ParseException Thrown when something bad happens while parsing
	 */
	public String onFunction(String function, List<String> args, int line) throws ParseException;
	
	/**
	 * Called when a function is to be executed with a single
	 * string value and then replaced in text
	 * @param function The function to call
	 * @param argument The argument value
	 * @param line The line on which this call originated
	 * @param col The column at which this call originated
	 * @return The return value
	 * @throws ParseException Thrown when something bad happens while parsing
	 */
	public String onInvocationReplacement(String function, String argument, int line, int col) throws ParseException;
}
