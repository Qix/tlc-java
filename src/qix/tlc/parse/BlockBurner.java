package qix.tlc.parse;

import java.io.IOException;
import java.io.InputStream;

/**
 * Burner that returns an entire block.
 * 
 * It is string conscious and takes care of
 * all escape patterns.
 * 
 * It is a tricky class, as each burn actually
 * creates an internal burner object that has
 * a per-burn state.
 * 
 * @author Qix
 *
 */
class BlockBurner implements IBurner {

	private final boolean processCurrent;
	
	BlockBurner(boolean processCurrent)
	{
		// Store
		this.processCurrent = processCurrent;
	}
	
	@Override
	public BurnResult burn(InputStream stream, char currentCharacter)
			throws IOException, ParseException {
		// Create new session
		BlockBurnerSession session = new BlockBurnerSession();
		
		// Burn + Return
		return session.burn(stream, currentCharacter);
	}

	/**
	 * The per-burn state class;
	 * does the heavy lifting for
	 * counting logic.
	 * 
	 * @author Qix
	 *
	 */
	private class BlockBurnerSession extends SimpleBurner
	{
		boolean inString = false; ///< Are we in a ' string?
		boolean inDString = false;///< Are we in a " string?
		int lastSlashCount = 0;   ///< The last group of slashes was this long
		boolean lastSlash = false;///< Was the last character a slash (i.e. are we counting slashes?)
		int depth = 1;            ///< Current depth
		
		BlockBurnerSession()
		{
			// Super
			super(BlockBurner.this.processCurrent);
		}

		@Override
		BurnAction getAction(char cha) {
			// Handle slashes
			this.handleSlash(cha);
			
			// Handle string
			this.handleString(cha);
			
			// Handle depth
			this.handleDepth(cha);
			
			// Zero depth?
			if(this.depth == 0)
				// This essentially means the last character was a '}' and to skip it
				return BurnAction.ABORT;
			else
				return BurnAction.CONTINUE;
		}
		
		/**
		 * Handles bracket counting (depth)
		 * @param cha The character to process
		 */
		private void handleDepth(char cha)
		{
			// Are we in a string?
			if(this.inDString || this.inString)
				return;
			
			// Is this a bracket?
			if(cha == '{')
				this.depth++;
			else if(cha == '}')
				this.depth--;
		}
		
		/**
		 * Handles strings (in/out, slashing)
		 * @param cha This character to process
		 */
		private void handleString(char cha)
		{
			// Is this a quote?
			if(cha == '\'')
			{
				// Are we currently in a double quote string?
				if(this.inDString)
					return;
				
				// Is this NOT escaped?
				if(this.lastSlashCount % 2 == 0)
					// Toggle flag
					this.inString = !this.inString;
			}
			
			// Is this a double quote?
			if(cha == '"')
			{
				// Are we currently in a single quote string?
				if(this.inString)
					return;
				
				// Is this NOT escaped?
				if(this.lastSlashCount % 2 == 0)
					// Toggle flag
					this.inDString = !this.inDString;
			}
		}
		
		/**
		 * Handles slashes (counting and flagging)
		 * @param cha The character to process
		 */
		private void handleSlash(char cha)
		{
			// Is this a slash?
			if(cha == '\\')
			{
				// Have we been counting slashes?
				if(this.lastSlash)
					// Increment slash count
					++this.lastSlashCount;
				else
				{
					// We are now.
					this.lastSlash = true;
					this.lastSlashCount = 1;
				}
			}else{
				// Have we been counting slashes?
				if(this.lastSlash)
				{
					// We're not now
					this.lastSlash = false;
				}else
					// Reset count
					this.lastSlashCount = 0;
			}
		}
		
		@Override
		void onFinish(InputStream stream, boolean hard, char lastChar) throws ParseException {
			// Hard?
			if(!hard) return;
			
			// Convert input stream
			CounterInputStream cStream = (CounterInputStream)stream;
			
			// Were we done?
			String notDone = null;
			if(this.inDString) notDone = "\"";
			if(this.inString) notDone = "'";
			if(this.depth > 0) notDone = String.format("} x%d", this.depth);
			
			// Throw
			if(notDone != null)
				throw new ParseException(
						cStream.getLine(),
						cStream.getCol(),
						"Unexpected EOL while parsing TLC source: expected: %s",
						notDone);
				
		}
	}
}
