package qix.tlc;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import qix.tlc.function.Function;
import qix.tlc.lua.LuaListener;
import qix.tlc.lua.LuaState;
import qix.tlc.parse.ITLCVisitor;
import qix.tlc.parse.ParseException;
import qix.tlc.parse.TLCParser;

/**
 * Class that processes a single source file.
 * 
 * @author Qix
 *
 */
public class TLCProcessor implements ITLCVisitor, LuaListener {
	
	private final LuaState state;
	private final TLCParser parser;
	private final File file;
	private final String parentDir;
	private final Set<File> dependencies = new HashSet<>();
	
	public TLCProcessor(Path path, boolean quiet) throws ParseException, IOException
	{
		// This
		this(path.toFile(), quiet);
	}
	
	public TLCProcessor(File file, boolean quiet) throws ParseException, IOException
	{
		// Store
		this.file = file;
		this.parentDir = this.file.getParent();
		this.state = new LuaState(this, quiet);
		
		// Does this have a side-by-side?
		File sbsFile = new File(file.toString().replaceAll("\\.[Tt][Ll][Cc]$", "") + ".lua");
		if(sbsFile.exists())
			// Do it
			this.state.doFile(sbsFile.getAbsoluteFile());
		
		// Get input stream
		try(InputStream stream = new FileInputStream(file))
		{
			// Parse + Visit
			this.parser = new TLCParser(stream, this);
		}
	}
	
	/**
	 * @return This processor's Lua state
	 */
	public LuaState getLua()
	{
		// Return
		return this.state;
	}
	
	/**
	 * @return A list of files this script needed in order to pre-process
	 */
	public Set<File> getDependencies()
	{
		// Return
		return Collections.unmodifiableSet(this.dependencies);
	}
	
	@Override
	public String toString()
	{
		// Return
		return this.parser.toString();
	}

	@Override
	public String onInvocation(String block, int line, int col) {
		// Invoke + Return
		return this.state.evaluate(block, this.getChunkName("eval", line, col));
	}

	@Override
	public String onReplacement(String symbol, int line, int col) {
		// Invoke + Return
		return this.state.evaluateReturn(symbol, this.getChunkName("replace", line, col));
	}

	@Override
	public String onFunction(String function, List<String> args, int line) throws ParseException {
		// Is there a function?
		Function func = null;
		try
		{
			func = Function.valueOf(function.toUpperCase());
		}catch(IllegalArgumentException e)
		{
			// Re-throw
			throw new ParseException(e, line, "No function by the name of '%s' exists", function);
		}
		
		// Check args
		func.checkArgs(this, args, line);
		
		// Invoke + Return
		return func.doFunction(this, args, line);
	}
	
	@Override
	public String onInvocationReplacement(String function, String argument,
			int line, int col) throws ParseException {
		// Format
		String command = String.format("%s([[%s]])",
				function,
				argument
				);
		
		// Return
		return this.state.evaluateReturn(command,
				this.getChunkName(
						String.format(
								"invoke-ret <%s>",
								function),
							line,
							col));
	}
	
	/**
	 * Generates a standardized chunk name
	 * @param tag A single word to describe what delim is being used
	 * @param line The line the chunk starts at
	 * @param col The column the chunk starts at
	 * @return A standardized chunk name for Lua
	 */
	private String getChunkName(String tag, int line, int col)
	{
		// Return
		return String.format(
				"%s: %s <l:%d,c:%d>",
				tag,
				this.file.toString(),
				line,
				col);
	}

	@Override
	public void onLuaDependency(File file) {
		// Add file
		this.dependencies.add(file);
	}

	@Override
	public String transformLuaImport(String path) {
		// Absolute?
		if(new File(path).isAbsolute())
			return path;
		
		// Create the path
		return Paths.get(
				this.parentDir,
				path).toString();
	}
}
