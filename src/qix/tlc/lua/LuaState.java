package qix.tlc.lua;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.luaj.vm2.Globals;
import org.luaj.vm2.LuaError;
import org.luaj.vm2.LuaValue;
import org.luaj.vm2.Varargs;
import org.luaj.vm2.lib.jse.JsePlatform;

import qix.tlc.TLCStandalone;

/**
 * Holds all Lua information and state
 * and facilitates easy loading of files
 * and execution of closures.
 * 
 * @author Qix
 *
 */
public class LuaState {

	/*
	 * Debug globals are enabled here just
	 * because there is no real security threat
	 * if the user knows what they're doing.
	 * 
	 * Change to `standardGlobals` if you're concerned.
	 */
	private final Globals globals = JsePlatform.debugGlobals();
	
	public LuaState(LuaListener listener, boolean quiet)
	{
		// Add all additional paths
		for(String p : TLCStandalone.getAdditionalPaths())
			// Add
			this.addIncludePath(p);
		
		// Also add some built-in paths
		this.addIncludePath("?.lua");
		
		// Apply Hooks
		HookDoFile.hook(this.globals, listener);
		HookLoadFile.hook(this.globals, listener);
		HookRequire.hook(this.globals, listener);
		if(quiet)
			HookMute.hook(this.globals);
		
		// Import all
		for(String i : TLCStandalone.getAutomaticImports())
			// Import
			this.importLua(i);
	}
	
	/**
	 * Invokes a file to be executed into the Lua state
	 * @param file The file to execute
	 */
	public void doFile(File file)
	{
		// Get doFile
		this.globals.get("dofile").invoke(LuaValue.valueOf(file.toString()));
	}
	
	/**
	 * Adds an include path to the modules path
	 * for the Lua project
	 * @param path The path to add
	 */
	public void addIncludePath(String path)
	{
		// Get pkg value
		LuaValue pkg = this.globals.get("package");
		
		// Create value
		String newValue = String.format("%s;%s",
				pkg.get("path").tojstring().replaceAll(";+$", ""),
				path.toString()
				);
		
		// Set
		pkg.set("path", newValue);
	}
	
	/**
	 * Evaluates a blob of Lua code
	 * @param code The code to evaluate
	 * @param name The name of the closure chunk (script name, C file, etc.)
	 * @return A string if there was a return code,
	 * or null for no return values
	 */
	public String evaluate(String code, String name) throws LuaError
	{
		// Generate closure
		Varargs closure = this.globals.baselib.loadStream(
				new ByteArrayInputStream(code.getBytes()), name, "bt",
				this.globals);
		
		// Parse error?
		if(closure.arg1().isnil())
			// Yep.
			throw new LuaError(closure.arg(2).tojstring());
		
		// Call
		Varargs result = ((LuaValue)closure).invoke();
		
		// Any args?
		if(result.narg() == 0)
			return null;
		else
			return result.arg1().tojstring();
	}
	
	/**
	 * Evaluates a line of Lua code with `return` pre-pended to it 
	 * @param code The code to evaluate
	 * @param name The name of the closure chunk (script name, C file, etc.)
	 * @return The return value or null if none
	 */
	public String evaluateReturn(String code, String name)
	{
		// Evaluate and return
		return this.evaluate(String.format("return tostring(%s)", code), name);
	}
	
	/**
	 * Imports a lua file as if `require` was called
	 * @param importString The import string to pass to require
	 */
	public String importLua(String importString)
	{
		// Get dofile
		LuaValue require = this.globals.get("require");
		
		// Execute
		Varargs result = require.invoke(LuaValue.valueOf(importString));
		
		// Result?
		if(result.narg() == 0)
			return null;
		else
			return result.arg1().tojstring();
	}
}
