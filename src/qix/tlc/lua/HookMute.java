package qix.tlc.lua;

import java.io.OutputStream;
import java.io.PrintStream;

import org.luaj.vm2.Globals;

/**
 * Mutes a Lua state.
 * 
 * This simply sets the global
 * output PrintStream (the STDOUT Lua
 * 'file descriptor') to something that
 * doesn't do anything! 
 * 
 * @author Qix
 *
 */
class HookMute {

	private static final PrintStream NULL_STREAM = new PrintStream(new OutputStream() {
		@Override
		public void write(int b) {}
	});
	
	/**
	 * Sets the globals' output stream to null
	 * @param globals The globals object to mute
	 */
	public static void hook(Globals globals)
	{
		// Mute
		globals.STDOUT = HookMute.NULL_STREAM;
	}
}
