package qix.tlc.function;

import java.util.List;

import qix.tlc.TLCProcessor;
import qix.tlc.parse.ParseException;

/**
 * Defines the methods for handling functions
 * 
 * @author Qix
 *
 */
public interface IFunction {
	
	/**
	 * Called as a preliminary check of all
	 * passed-in arguments
	 * @param processor The processor making the call
	 * @param args The arguments to check
	 * @param line The line of the function
	 * @throws ParseException Thrown if something is wrong with the passed arguments
	 */
	public void checkArgs(TLCProcessor processor, List<String> args, int line) throws ParseException;
	
	/**
	 * Performs the action of the function
	 * @param processor The processor making the call
	 * @param args The arguments to use
	 * @param line The line of the function call
	 * @return What should be used to replace the function delimiter
	 * @throws ParseException Thrown when something goes wrong in the parsing
	 */
	public String doFunction(TLCProcessor processor, List<String> args, int line) throws ParseException;
}
