package qix.tlc.args;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import qix.tlc.TLCStandalone;
import qix.tlc.args.Option.ArgumentRequirement;

public class ArgumentVisitor {
	
	private final IArgumentHandler handler;
	private final Map<Character, Flag> handlerMapFlags = new HashMap<>();
	private final Map<Flag, Method> handlerMethodMapFlags = new HashMap<>();
	private final Map<String, Option> handlerMapOptions = new HashMap<>();
	private final Map<Option, Method> handlerMethodMapOptions = new HashMap<>();
	private final Set<String> sentOptions = new HashSet<>();
	private final Set<Character> sentFlags = new HashSet<>();
	
	public ArgumentVisitor(IArgumentHandler handler)
	{
		// Store
		this.handler = handler;
		
		// Setup
		this.initializeHandler();
	}
	
	/**
	 * Processes all of the arguments in a string
	 * array
	 * @param args The argument array to process
	 */
	public void visit(String[] args)
	{
		// Iterate
		for(String arg : args)
			this.process(arg);
	}
	
	/**
	 * Performs a black magic voodoo ritual on
	 * the supplied handler to find all visitor
	 * annotated methods
	 */
	private void initializeHandler()
	{
		// Get class
		Class<? extends IArgumentHandler> clazz = this.handler.getClass();
		
		// Iterate methods
		for(Method method : clazz.getDeclaredMethods())
		{
			// Has flag annotation?
			Annotation annot = method.getAnnotation(Flag.class);
			if(annot != null)
			{
				// Add to map
				this.handlerMethodMapFlags.put((Flag) annot, method);
				this.handlerMapFlags.put(((Flag)annot).value(), (Flag)annot);
			}
			
			// Has option annotation?
			annot = method.getAnnotation(Option.class);
			if(annot != null)
			{
				// Add to map
				this.handlerMethodMapOptions.put((Option) annot, method);
				this.handlerMapOptions.put(((Option)annot).value(), (Option)annot);
			}
		}
	}
	
	/**
	 * Parses and sends off an argument to the
	 * correct place
	 * @param argument The argument to handle
	 */
	private void process(String argument)
	{
		// Trim
		argument = argument.trim();
		
		// Is it empty?
		if(argument.length() == 0)
			return;
		
		// Does it start with 2 slashes?
		if(argument.matches("^\\-\\-[^\\-].+$"))
		{
			// Adjust
			argument = argument.substring(2);
			
			// Split
			String[] parts = argument.split("=", 2);
			
			// Validate parts
			if(parts.length == 2)
			{
				// Is the argument length 0?
				if(parts[1].length() == 0)
					// Just eliminate the second part
					parts = new String[]{parts[0]};
				else
					// Spruce up the second part
					parts[1] = parts[1]
								.replaceAll("^\"", "") // Replace any beginning quote
								.replaceAll("(.*[^\\\\](\\\\\\\\)*)\"$", "$1"); // Replace any non-escaped ending quote
			}
			
			// Get option
			Option option = this.handlerMapOptions.get(parts[0]);
			if(option == null)
				// Throw
				throw new IllegalArgumentException(
						String.format(
								"Unknown option: --%s",
								parts[0]));
			
			// Check Unique + Already sent
			if(option.unique() && this.sentOptions.contains(option.value()))
				// Throw
				throw new IllegalArgumentException(
						String.format(
								"Option can only be specified once: --%s",
								parts[0]));
			
			// Check argument status
			switch(option.argument())
			{
			case ABSENT:
				if(parts.length == 2)
					// Throw
					throw new IllegalArgumentException(
							String.format(
									"Option does not call for argument: --%s",
									parts[0]));
				break;
			case REQUIRED:
				if(parts.length == 1)
					// Throw
					throw new IllegalArgumentException(
							String.format(
									"Option requires argument: --%s",
									parts[0]));
				break;
			case OPTIONAL:
				// Swallow
				break;
			}
			
			// Get method
			Method method = this.handlerMethodMapOptions.get(option);
			
			// Invoke
			try {
				if(option.argument() == ArgumentRequirement.ABSENT)
					method.invoke(this.handler);
				else
					method.invoke(this.handler, parts.length == 2 ? parts[1] : null);
			} catch(InvocationTargetException e) {
				// Re-throw exception if necessary
				if(e.getCause() != null)
				{
					if(e.getCause() instanceof RuntimeException)
						throw (RuntimeException)e.getCause();
					else
						throw new RuntimeException(e.getCause());
				}	
				
				// Just re-throw otherwise
				throw new RuntimeException(e);
			} catch (IllegalAccessException | IllegalArgumentException e) {
				// Re-throw (This should never happen)
				throw new RuntimeException(e);
			}
			
			// Flag sent if unique
			if(option.unique())
				// Add
				this.sentOptions.add(option.value());
			
		}else if(argument.matches("^\\-[a-zA-Z0-9]+$"))
		{
			// Adjust
			argument = argument.substring(1);
			
			// Iterate characters
			for(char cha : argument.toCharArray())
			{
				// Get flag
				Flag flag = this.handlerMapFlags.get(cha);
				if(flag == null)
					// Throw
					throw new IllegalArgumentException(
							String.format(
									"Unknown flag: -%c",
									cha));
				
				// Check if we've already visited
				if(this.sentFlags.contains(cha))
					// Throw
					throw new IllegalArgumentException(
							String.format(
									"Flag can only be specified once: -%c",
									cha));
				
				// Get method
				Method method = this.handlerMethodMapFlags.get(flag);
				
				// Invoke
				try {
					method.invoke(this.handler);
				} catch (IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					// Re-throw (This should never happen)
					throw new RuntimeException(e);
				}
				
				// Flag sent
				this.sentFlags.add(cha);
			}
		}else
			// Malformed
			this.handler.onMalformed(argument);
	}
	
	@Override
	public String toString() {
		// Create buffer
		StringBuffer buffer = new StringBuffer();

		// Initial stuff
		buffer.append(
				String.format(
						"%s%n%nUSAGE%n  tlc [flags] [options] file1[ file2 [fileN]]%n",
						TLCStandalone.getVersionString(true)));
		
		// Iterate the flags
		buffer.append(String.format("%nFLAGS%n"));
		final String flagPadding = new String(new byte[25]).replace('\0', ' ');
		for(Flag f : this.handlerMapFlags.values())
			// Generate and add
			buffer.append(
					String.format(
							"  -%c%s%s%n",
							f.value(),
							flagPadding,
							f.message()));
		
		// Iterate the options
		buffer.append(String.format("%nOPTIONS%n"));
		for(Option o : this.handlerMapOptions.values())
		{
			// Generate argument string
			String argString = null;
			switch(o.argument())
			{
			case ABSENT: argString = ""; break;
			case OPTIONAL: argString = "=[...]"; break;
			case REQUIRED: argString = "=..."; break;
			}
			
			// Generate option text
			String optionText = String.format("--%s%s",
					o.value(),
					argString);
			
			// Calculate / generate padding
			int paddingLen = 25 - optionText.length();
			if(paddingLen < 2)
				throw new RuntimeException("Help text padding less than two for: " + o.value());
			String padding = new String(new byte[paddingLen]).replace('\0', ' ');
			
			// Generate + Add final entry
			buffer.append(String.format(
					"  %s%s  %s%n",
					optionText,
					padding,
					o.message()
					));
		}
		
		// Return
		return buffer.toString();
	}
}
