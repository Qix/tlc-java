package qix.tlc.args;

public interface IArgumentHandler {
	/**
	 * Called when an argument is passed
	 * that doesn't start with 1 or two slashes.
	 * 
	 * This can be used for files being passed or other
	 * default argument types.
	 * @param value The 'ill formed' value that was passed
	 */
	public void onMalformed(String value);
}
