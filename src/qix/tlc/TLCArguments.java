package qix.tlc;

import java.io.File;
import java.io.IOException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import qix.tlc.args.Flag;
import qix.tlc.args.IArgumentHandler;
import qix.tlc.args.Option;
import qix.tlc.args.Option.ArgumentRequirement;

/**
 * Lists the arguments the TLC Standalone program
 * recognizes, and handles them when passed.
 * 
 * @author Qix
 *
 */
public final class TLCArguments implements IArgumentHandler
{
	private final Set<File> files = new LinkedHashSet<File>();
	private boolean help = false;
	private boolean version = false;
	private boolean verbose = false;
	private boolean forceOverwrite = false;
	private boolean preservePath = false;
	private boolean makeDirs = false;
	private boolean testMode = false;
	private boolean showDepends = false;
	private boolean bareFiles = false;
	private Path dependsPrefix = null;
	private Path prefix = Paths.get("./");
	
	@Override
	public void onMalformed(String value) {
		// Try to get as a file
		File file = new File(value);
		
		// Non-existent?
		if(!file.exists() || !file.isFile())
			// Throw
			throw new IllegalArgumentException(
					String.format(
							"Not a file: %s",
							value));
		
		// Add
		this.files.add(file);
	}
	
	/**
	 * @return Whether or not to only show the destination files
	 * when verbose mode (-v) is enabled and listing files
	 */
	public boolean isBareFormat()
	{
		// Return
		return this.bareFiles;
	}
	
	/**
	 * @return The directory that all dependencies are relativized against
	 */
	public Path getDependsPrefix()
	{
		// Return
		return this.dependsPrefix;
	}
	
	/**
	 * @return Whether or not the user is requesting a dependency
	 * list instead of a generation
	 */
	public boolean isDependencyList()
	{
		// Return
		return this.showDepends;
	}
	
	/**
	 * NOTE: TLC essentially does nothing if -v is not
	 * supplied with -t!
	 * @return Whether or not to only show files (but not generate anything)
	 */
	public boolean isTestMode()
	{
		// Return
		return this.testMode;
	}
	
	/**
	 * @return Whether or not to preserve the path of the file in the prefix
	 */
	public boolean isPathPreserved()
	{
		// Return
		return this.preservePath;
	}
	
	/**
	 * NOTE: Directory structures that are not existent will cause errors.
	 * This is useful with -p
	 * @return Whether or not TLC should make the parent directory structure before processing
	 */
	public boolean isParentPathCreated()
	{
		// Return
		return this.makeDirs;
	}
	
	/**
	 * @return Whether or not the user requested a help message
	 */
	public boolean isHelpRequest()
	{
		// Return
		return this.help;
	}
	
	/**
	 * @return Whether or not identical input/output paths should force overwrite
	 */
	public boolean isForcedOverwrite()
	{
		// Return
		return this.forceOverwrite;
	}
	
	/**
	 * @return Whether or not the user requested a version string
	 */
	public boolean isVersionRequest()
	{
		// Return
		return this.version;
	}
	
	/**
	 * @return Whether or not to print each file processed
	 */
	public boolean isVerbose()
	{
		// Return
		return this.verbose;
	}
	
	/**
	 * @return Gets the output prefix
	 */
	public Path getPrefix()
	{
		// Return
		return this.prefix;
	}
	
	@Option(value="version", message="Shows TLC's version")
	public void doVersion()
	{
		// Set
		this.version = true;
	}
	
	@Option(value="help", message="Shows this help message")
	public void doHelp()
	{
		// Set
		this.help = true;
	}
	
	@Option(value="prefix", argument=ArgumentRequirement.REQUIRED, message="Sets the output prefix (ignored for absolute inputs)")
	public void doPrefix(String prefix)
	{
		try
		{
			// Get path
			Path path = Paths.get(prefix);
			
			// Does it exist?
			if(!path.toFile().exists())
				// Throw
				throw new IllegalArgumentException(
						String.format(
								"Prefix path does not exist: %s",
								prefix));
			
			// Is it a directory?
			if(!path.toFile().isDirectory())
				// Throw
				throw new IllegalArgumentException(
						String.format(
								"Prefix path not a directory: %s",
								prefix));
			
			// Store
			this.prefix = path;
		}catch(InvalidPathException e)
		{
			// Re-throw
			throw new IllegalArgumentException(
					String.format(
							"Prefix path not valid (%s): %s",
							e.getMessage(),
							prefix));
		}
	}
	
	@Option(value="dependency-prefix",
			argument=ArgumentRequirement.REQUIRED,
			message="The directory that all dependencies are relativized against when listed (-D)")
	public void doSetDependsPrefix(String value)
	{
		try
		{
			// Get file
			File file = new File(value);
			
			// Absolute?
			if(file.isAbsolute())
				// Set
				this.dependsPrefix = file.toPath();
			else
			{
				// Get path
				Path dirtyPath = Paths.get(
						System.getProperty("user.dir"),
						value);
				
				// Get file + canonicalize + store
				this.dependsPrefix =
						dirtyPath.toFile().getCanonicalFile().toPath();
			}
		}catch(InvalidPathException e)
		{
			// Throw
			throw new IllegalArgumentException(
					String.format(
							"Dependency prefix path not valid (%s): %s",
							e.getMessage(),
							prefix));
		}catch(IOException e)
		{
			// Throw
			throw new IllegalArgumentException(
					String.format(
							"Could not evaluate dependency prefix path (%s): %s",
							e.getMessage(),
							prefix));
		}
	}
	
	@Option(value="import-dir",
			argument=ArgumentRequirement.REQUIRED,
			unique=false,
			message="Adds an import directory for Lua @import statements")
	public void doAddImport(String value)
	{
		// Add
		TLCStandalone.addAdditionalPath(value);
	}
	
	@Option(value="use",
			argument=ArgumentRequirement.REQUIRED,
			unique=false,
			message="Imports a module")
	public void doImport(String value)
	{
		// Add
		TLCStandalone.addAutomaticImport(value);
	}
	
	@Flag(value='v', message="Prints each processed file")
	public void doVerbose()
	{
		// Set
		this.verbose = true;
	}
	
	@Flag(value='b', message="Prints each file in bare (destination only) format (using -v)")
	public void doBare()
	{
		// Set
		this.bareFiles = true;
	}
	
	@Flag(value='f', message="Forces overwriting of identical input/output paths")
	public void doForcedOverwrite()
	{
		// Set
		this.forceOverwrite = true;
	}
	
	@Flag(value='p', message="Creates parent directories of output files")
	public void doMakeParent()
	{
		// Set
		this.makeDirs = true;
	}
	
	@Flag(value='P', message="Preserves paths when generating output files")
	public void doPreservePath()
	{
		// Set
		this.preservePath = true;
	}
	
	@Flag(value='t', message="Test mode (doesn't generate output files)")
	public void doTestMode()
	{
		// Set
		this.testMode = true;
	}
	
	@Flag(value='D', message="Shows which scripts certain file(s) depend upon; doesn't generate")
	public void doShowDependencies()
	{
		// Set
		this.showDepends = true;
	}
	
	/**
	 * @return The files passed in from the command line
	 */
	public Set<File> getFiles()
	{
		// Return
		return Collections.unmodifiableSet(this.files);
	}
}